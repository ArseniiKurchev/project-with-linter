class TextStorage:
    """Класс для чтения и хранения текста."""

    def __init__(self, path_to_text: str):
        self.lines: list = self._read_lines_from_text(path_to_text)

    def _read_lines_from_text(self, path_to_text: str) -> list:
        with open(path_to_text) as text_file:
            lines = text_file.readlines()
            return [line.rstrip() for line in lines]


class RussianPhoneChecker:
    """Класс, реализующий проверку на номер РФ."""

    def is_russian_number_check(self, phone: str, to_print=True) -> bool:
        """Метод проверки телефонного номера."""
        is_russian_phone = False
        if phone.startswith('+'):
            phone = phone[1:]
        if not phone.isdigit():
            is_russian_phone = False
        if phone.startswith('79') or phone.startswith('89'):
            is_russian_phone = True
        if to_print:
            self._log_result(is_russian_phone)
        return is_russian_phone

    def _log_result(self, is_russian_phone: bool):
        if is_russian_phone:
            print('Телефонный номер является номером РФ.')
        else:
            print('Телефонный номер не является номером РФ.')


class BlacklistCheck:
    """Класс, проверяющий нет ли в заданного телефона в черном списке."""

    def __init__(self, bl_list: list):
        self.blacklist = bl_list

    def blacklist_check(self, phone: str, to_print=True) -> bool:
        """Метод проверки нахождения номера в черном списке."""
        blacklisted = False
        if phone.startswith('+7'):
            phone = phone[2:]
        elif phone.startswith('8'):
            phone = phone[1:]
        if phone in self.blacklist:
            blacklisted = True
        if to_print:
            self._check_result(blacklisted)
        return blacklisted

    def _check_result(self, blacklisted: bool):
        if blacklisted:
            print('Телефонный номер находится в черном списке')
        else:
            print('Телефонный номер не находится в черном списке')


def logging_decorator(func):
    """Функция, сохраняющая построчно номер и результаты проверки в отдельный файл."""
    def wrapper(phone: str, bl: str):
        func(phone, bl)
        ru_check = RussianPhoneChecker().is_russian_number_check(phone, to_print=False)
        bl_check = BlacklistCheck(TextStorage(bl).lines).blacklist_check(phone, to_print=False)

        log_file = 'log.csv'
        with open(log_file, 'a') as log:
            log.write(f'{phone};{ru_check};{bl_check};\n')

    return wrapper


@logging_decorator
def phone_check(phone: str, bl: str):
    """Комплексная проверка номера."""
    ru_checker = RussianPhoneChecker()
    ru_checker.is_russian_number_check(phone)

    blacklist = TextStorage(bl)

    bl_checker = BlacklistCheck(blacklist.lines)
    bl_checker.blacklist_check(phone)


if __name__ == '__main__':
    phone = '+79529220099'
    bl = 'blacklist.txt'

    phone_check(phone, bl)
